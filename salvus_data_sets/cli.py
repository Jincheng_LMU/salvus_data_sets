"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import pathlib
import sys
import typing

import click
import numpy as np
import obspy
import pandas

from .remote_data import fetch, SALVUS_REMOTE_DATA


@click.group()
@click.option("--pdb", help="Enable post-mortem debugger.", is_flag=True)
def main(pdb):
    """
    SalvusDataSets
    """
    # Enable post-mortem debugger hook.
    if pdb:

        def info(type, value, tb):
            if hasattr(sys, "ps1") or not sys.stderr.isatty():
                # we are in interactive mode or we don't have a tty-like
                # device, so we call the default hook
                sys.__excepthook__(type, value, tb)
            else:
                import traceback, pdb as debugger  # NOQA

                traceback.print_exception(type, value, tb)
                debugger.post_mortem(tb)

        sys.excepthook = info


def _event_to_dict(event: obspy.core.event.Event) -> typing.Dict:
    origin = event.preferred_origin()
    tensor = event.preferred_focal_mechanism().moment_tensor.tensor
    magnitude = event.preferred_magnitude()

    # Break it down to the minimum information required for SalvusProject to
    # use it.
    return {
        "latitude": origin.latitude,
        "longitude": origin.longitude,
        "depth_in_meters": origin.depth,
        "origin_time": np.datetime64(origin.time),
        "moment_magnitude": magnitude.mag,
        "mrr": tensor.m_rr,
        "mtt": tensor.m_tt,
        "mpp": tensor.m_pp,
        "mrt": tensor.m_rt,
        "mrp": tensor.m_rp,
        "mtp": tensor.m_tp,
    }


@main.command("build-gcmt-catalog-file")
@click.option(
    "--output-filename",
    type=click.Path(),
    required=True,
    help="The output filename. Must not exist yet.",
)
def build_gcmt_catalog_file(output_filename):
    output_filename = pathlib.Path(output_filename)
    if output_filename.exists():
        raise ValueError("Output file must not yet exist.")

    output_filename.parent.mkdir(exist_ok=True)

    # Get all the GCMT keys from the registry.
    all_keys = []
    for key in SALVUS_REMOTE_DATA.registry.keys():
        if not key.startswith("GCMT"):
            continue
        all_keys.append(key)

    cat = obspy.Catalog()

    for k in all_keys:
        print(f"Reading {k}...")
        cat += obspy.read_events(str(fetch(k)), format="ndk")

    df = pandas.DataFrame([_event_to_dict(e) for e in cat])
    df.to_csv(output_filename)
